<?php
namespace Geocode;

class GeocodeResult
{

	private $results;

	private $status;

	private $errorMessage;

	/**
	 * create GeocodeResult from json
	 * @param \stdClass $json
	 * @return self
	 */
	public static function fromJson($json)
	{
		$result = new self();
		$result
			->setResults($json->results)
			->setStatus($json->status);

		if (isset($json->error_message)) {
			$result->setErrorMessage($json->error_message);
		}

		return $result;
	}

	/**
	 * @return mixed
	 */
	public function getResults()
	{
		return $this->results;
	}

	/**
	 * @param mixed $results
	 * @return self
	 */
	public function setResults($results)
	{
		$this->results = $results;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return self
	 */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage()
	{
		return $this->errorMessage;
	}

	/**
	 * @param string $errorMessage
	 * @return self
	 */
	public function setErrorMessage($errorMessage)
	{
		$this->errorMessage = $errorMessage;
		return $this;
	}

}
