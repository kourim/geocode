<?php
namespace Geocode;

class Geocode
{

	/**
	 * list of proxies ip adresses
	 * @var string[]
	 */
	private $proxies = [
		'195.3.113.170:3128',
		'89.163.131.168:3128',
		'149.202.249.227:3128',
	];

	/**
	 * list of proxies, which are using
	 * @var string[]
	 */
	private $usedProxies = [];

	/**
	 * list of banned proxies for date
	 * @var string[]
	 */
	private $bannedProxies = [];

	/**
	 * geocode url address
	 * @var string
	 */
	private $geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?&address=%s&language=%s';

	/**
	 * default geocode language
	 * @var string
	 */
	private $language = 'cs';

	/**
	 * default timeout of one request, in miliseconds
	 * @var int
	 */
	private $timeout = 5000;

	/**
	 * number of tries before ban
	 * @var int
	 */
	private $timeoutMax = 3;

	/**
	 * list of timeouts count for every proxy
	 * @var string[]
	 */
	private $timeoutCount = [];

	/**
	 * debug mode
	 * @var bool
	 */
	private $debugMode = false;

	/**
	 * Geocode constructor.
	 * @param string[] $proxies list of proxy ips in format ipv4:port
	 * @param int $timeout timeout for requests, in miliseconds
	 * @param string $language
	 */
	public function __construct($proxies, $timeout = 5000, $language = 'cs')
	{
		// proxies
		$this->proxies = $proxies;
		foreach ($this->proxies as $proxy) {
			$this->usedProxies[$proxy] = $proxy;
		}

		$this->timeout = $timeout;
		$this->language = $language;
	}

	/**
	 * get geocode result over random proxy, if proxy is used to max, skip for another one
	 * @param string $address
	 * @throws GeocodeException
	 * @return GeocodeResult
	 */
	public function getGeocode($address)
	{
		while (true) {
			// get random proxy
			$randomProxy = $this->getRandomProxy();

			// process address in geocode
			try {
				$this->debug("Making API call for address '".$address."' over ".$randomProxy);
				$result = $this->processAddress($address, $randomProxy);

				// if ip address gets banned
				if ($result->getStatus() == 'OVER_QUERY_LIMIT') {
					$this->banProxy($randomProxy);
					continue;
				}

				// if we get result, end it
				return $result;
			} catch (GeocodeException $e) {
				$this->debug($e->getMessage());

				// check if its timeout exception, if so, ban proxy after $timeoutMax tries
				if ($e->getCode() == 28) {
					if (!isset($this->timeoutCount[$randomProxy])) {
						$this->timeoutCount[$randomProxy] = 0;
					}
					$this->timeoutCount[$randomProxy]++;
					if ($this->timeoutCount[$randomProxy] > $this->timeoutMax) {
						$this->banProxy($randomProxy);
					}
				}

				continue;
			}
		}

		throw new GeocodeException("Cannot get geocode address from google");
	}

	/**
	 * get random proxy from list, which is not banned
	 * @throws GeocodeException
	 * @return string
	 */
	private function getRandomProxy()
	{
		if (count($this->usedProxies) < 1) {
			throw new GeocodeException("All proxies hase been used and banned, we cannot continue");
		}
		$proxy = $this->usedProxies[array_rand($this->usedProxies)];
		$this->debug("Choosing random proxy ".$proxy);
		return $proxy;
	}

	/**
	 * remove proxy from list of using proxies
	 * @param $proxy
	 */
	private function banProxy($proxy)
	{
		$this->debug("Banning proxy ".$proxy);
		$this->bannedProxies[$proxy] = $proxy;
		unset($this->usedProxies[$proxy]);
	}

	/**
	 * get geocode address from google with google and random proxy
	 * return google geocode json
	 * @param string $address
	 * @param string $proxy
	 * @return GeocodeResult
	 * @throws GeocodeException
	 */
	private function processAddress($address, $proxy)
	{
		// generate url
		$url = sprintf(
			$this->geocodeUrl,
			urlencode($address),
			$this->language
		);

		// create curl connections
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $this->timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, $this->timeout);

		$result = curl_exec($ch);
		if ($result !== false) {
			curl_close($ch);
			$tmp = json_decode($result);
			if ($tmp === null) {
				throw new GeocodeException("Cannot deserialize json from string '".$result."'", 0);
			}
			return GeocodeResult::fromJson($tmp);
		} else {
			$message = "Curl error: ".curl_error($ch);
			$errNumber = curl_errno($ch);
			curl_close($ch);
			throw new GeocodeException($message, $errNumber);
		}
	}

	/**
	 * start/stop debug mode
	 * @param bool $start
	 */
	public function debugMode($start = true)
	{
		$this->debugMode = (bool)$start;
	}

	/**
	 * just debug output
	 * @param string $text
	 */
	private function debug($text)
	{
		if ($this->debugMode) {
			echo date("Y-m-s H:i:s").": ".$text."\n";
		}
	}

}
