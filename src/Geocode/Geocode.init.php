<?php

/**
 * import of Geocode classes
 */

if (defined("GEOCODE_INIT_LOADED")) {
	return;
}

define("GEOCODE_INIT_LOADED", true);

require(dirname(__FILE__)."/Geocode.php");
require(dirname(__FILE__)."/GeocodeException.php");
require(dirname(__FILE__)."/GeocodeResult.php");