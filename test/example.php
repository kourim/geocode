<?php

include("../src/Geocode/Geocode.init.php");

try {
	$geocode = new \Geocode\Geocode(['195.3.113.170:3128', '89.163.131.168:3128', '149.202.249.227:3128']);
	$geocode->debugMode();
	$result = $geocode->getGeocode("Václavské náměstí 8, Praha 1");
	var_export($result);
} catch (\Geocode\GeocodeException $e) {
	echo $e->getMessage()."\n";
}
